#!/bin/bash

# NOTES
#
# * Instead of having both startTime and endTime hours, it's easier simply to
#	have cron execute every hour at a few minutes past the hour and see if
#	the current hour matches the startTime.  This means that startTime will
#	will always indicate the hour of execution. Also, before execution the numeric day 
#   will be checked against the person(numericDay) column to see if a matched was made.
#

## User-defined values ##
EMAIL_TITLE="message from tim"
MYSQL_HOST="localhost"
#MYSQL_HOST="198.101.157.250"
MYSQL_USER="tim"
MYSQL_PASSWORD='';
MYSQL_DB="remindthem"
CUR_TZ_OFFSET=-5

CUR_HR="`date +'%H'`"

# query all entries from 'people' that are valid
#	and where current hour is same as startTime, offset for TZ
a=`expr $CUR_HR - $CUR_TZ_OFFSET`

# Converts queries returning >1 line into newline-delimited array
ORIG_IFS=$IFS
IFS=$'\x0A'$'\x0D'

PERSONLIST=( `mysql -ss -h $MYSQL_HOST -u $MYSQL_USER -pb8M32pA6fSsyAbKw -e "SELECT CONCAT_WS(';', pid, uid, name, phone, email, type, birthday) FROM person WHERE (startTime - timezone) = $a;" $MYSQL_DB` )


# for each person scheduled this hour:
for p in $PERSONLIST
do
	# Divide string into array
	a=( `echo $p | tr ";" "\n"` )
	# Separate array into vars
	PID="${a[0]}"
	PUID="${a[1]}"
	NAME="${a[2]}"
	PHONE="${a[3]}"
	EMAIL="${a[4]}"
	TYPE="${a[5]}" # 0 => Whats up, 1 => Birthday
	BDAY="${a[6]}"
	
	# if birthday, then assert today's date matches birthday date
	#echo "comparing: `date +"%Y-%m-%d"` != $BDAY"
	if [[ "$TYPE" == "1" && "`date +\"%Y-%m-%d\"`" != "$BDAY" ]]
	then
		echo "Today is not their birthday; skipping!"
		continue
	fi
	
	#debug
	echo "doing: pid, uid, name, phone, email, type, bday: $PID, $PUID, \"$NAME\", $PHONE, $EMAIL, $TYPE, $BDAY"
	
	# select random message
	MSG=`mysql -ss -h $MYSQL_HOST -u $MYSQL_USER -pb8M32pA6fSsyAbKw -e "SELECT CONCAT_WS(';', mid, message) FROM message WHERE message_type = $TYPE AND uid = $PUID ORDER BY RAND() LIMIT 1;" $MYSQL_DB`

	a=( `echo $MSG | tr ";" "\n"` )
	MID="${a[0]}"
	MSG_CONTENT="${a[1]}"
	NAME="Tim"
	DATE=$(date +"%u")
	
	#echo "mid: $MID From:$NAME Content: $MSG_CONTENT"
	
	# Use numeric day to get current day of the week (1..7) 1 is monday
	if [[ "$DATE" == "6" ]]
	then
		# if 10-digit phone number
		if [[ ${#PHONE} -ge 10 ]]
		then
			# send SMS
			php "/var/www/test.terrelljordan.com/sms.php" "$PHONE" "$MSG_CONTENT"
			echo "Send SMS"
		# if invalid/undefined phone number, then send EMAIL
		else
			# send msg via PHP mail
			php "/var/www/test.terrelljordan.com/sendmail.php" "$EMAIL" "$MSG_CONTENT" "NAME"
			echo "Send msg via PHP mail"
			#echo "<?php $headers = 'From: ttjordan81@gmail.com' . \"\\r\\n\" . 'Reply-To: ttjordan81@gmail.com' . \"\\r\\n\" . 'X-Mailer: PHP' . phpversion(); mail('$EMAIL', '$EMAIL_TITLE', \"$MSG_CONTENT\", \$headers); ?>" | php
		fi
	else
		echo "Date do not match!"
	fi
	# write log entry
	mysql -ss -h $MYSQL_HOST -u $MYSQL_USER -pb8M32pA6fSsyAbKw -e "INSERT INTO log (mid, pid) VALUES ($MID, $PID);" $MYSQL_DB
done

exit 0

