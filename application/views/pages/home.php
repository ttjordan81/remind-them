<!DOCTYPE html>
<html>
  <head>
    <title>Remind Them</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    	<!-- Bootstrap -->
		    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
	
	<script src="http://code.jquery.com/jquery.js"></script>		  
	<script src="bootstrap/js/bootstrap.min.js"></script>

	
	<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js" type="text/javascript"></script>
   <!-- Le styles -->
    <style>
      body {
        padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
      }
	  .error{color:red;}
    </style>
<script type="text/javascript">

	$(document).ready(function(){
		$("#messageformValidate").validate();
	});
	$(document).ready(function(){
		$("#birthdayformValidate").validate();
	});
	
/* 	 function getID(passID){
		//var test = document.getElementById("demo").innerHTML=passID;
		document.getElementById('passhiddenID').value = passID;		
	} */
</script>
<script type="text/javascript">
		function getID(passID){
		//Pass id to input Model, so this can be edit.
		document.getElementById('passhiddenID').value = passID;	
		
          if(passID == ""){
            document.getElementById("entries").innerHTML="test"
            return;
          }
          if (window.XMLHttpRequest){
		  // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlHttp=new XMLHttpRequest();
          }
          else{// code for IE6, IE5
            xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
          }
          xmlHttp.onreadystatechange = function(){
            if(xmlHttp.readyState == 4 && xmlHttp.status == 200){
              document.getElementById("entries").innerHTML=xmlHttp.responseText; 
            }
          }
          xmlHttp.open("GET","index.php/page/get_message_whatsup?status="+passID,true);
          xmlHttp.send();
        }
</script>
  </head>
  <body>

    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="brand" href="#">Remind Them</a>
          <div class="nav-collapse collapse">
            <ul class="nav">
              <li class="active"><a href="#">How it Works!</a></li>
              <li><a href="#about">About</a></li>
              <li><a href="#contact">Contact</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container">
<!--Alerts -->		
<?php echo $this->session->flashdata('update'); ?>

      <h1>Just remind them by setting a comment up!</h1>
		<ul class="nav nav-tabs" id="myTab">
		  <li class="active"><a href="#person">Person</a></li>
		  <li><a href="#messages">Messages</a></li>
		  <li><a href="#settings">Settings</a></li>
		</ul>
		 
		<div class="tab-content">
			<div class="tab-pane active" id="person">
			
			<!-- Button to trigger Person modal -->
			<a href="#myModal-person" role="button" class="btn btn-large btn-primary"  data-toggle="modal">+Add Person</a>
 
			<br />
			<br />
			<table class="table table-bordered">
				<th>Name</th>
				<th>Phone</th>
				<th>Email</th>
				<th>Edit</th>
					<?php 
							foreach($get_person->result() as $row): ?>
							<tr>
								<td><?php echo $row->name; ?></td>
								<td><?php echo $row->phone; ?></td>
								<td><?php echo $row->email; ?></td>
								<td width="50">
								
								<div class="tags_select">
		
								<button class="btn btn-primary" type="button" onclick="getID('<?php echo $row->pid;?>')" data-toggle="modal" data-target="#myModal-edit-person">Edit</button>
						
								</div>
							
								</td>
							</tr>
						<?php endforeach; ?>
			</table>
			</div>
			
			<div class="tab-pane" id="messages">			
			<!-- Button to trigger message modal -->
			<a href="#myModal-message" role="button" class="btn btn-large btn-primary"  data-toggle="modal">+Add Message</a>
 
			<br />
			<br />
			
			<div class="tabbable tabs-left">
			  <ul class="nav nav-tabs" id="myTab2">
				<li class="active"><a href="#whatsup" data-toggle="tab">What's UP!</a></li>
				<li><a href="#birthday" data-toggle="tab">Birthday</a></li>
			  </ul>
			  
			<div class="tab-content" >
				<div class="tab-pane active"  id="whatsup">
					<table class="table table-bordered">
					<th>What's up Message</th>
						<?php 
							foreach($whatsup_message->result() as $row): ?>
							<tr>
								<td><?php echo substr($row->message, 0,105).'...'; ?></td>
								<td width="50">
								
								<div class="tags_select">
		
							<button class="btn btn-primary" type="button" onclick="getID('<?php echo $row->mid;?>')" data-toggle="modal" data-target="#myModal-edit-message">Edit</button>
						
								</div>
							
								</td>
							</tr>
						<?php endforeach; ?>
					</table>
				</div>
				<div class="tab-pane" id="birthday">
					<table class="table table-bordered">
					<th>Birthday Message</th>
					<tr>
						<td>...</td>
						<td width="50"><a class="btn btn-primary" href="#myModal-edit-birthday">Edit</a></td>
					</tr>
					</table>
				</div>
				</div>
			</div>

			</div>
			<div class="tab-pane" id="settings"> <?php echo $setting_view; ?> </div>
		</div>
		 
	<script>
    $(document).ready(function(){
        $('#myTab a').click(function (e){
            e.preventDefault();
        $(this).tab('show');
        })
    });
	
	    $(document).ready(function(){
        $('#myTab2 a').click(function (e){
            e.preventDefault();
        $(this).tab('show');
        })
    });
		
	</script>
    </div> <!-- /container -->

<!-- Person Modal -->
<div id="myModal-person" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
 <form action="<?php echo site_url("page/person");?>" method="POST" id="messageformValidate">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
    <h3 id="myModalLabel">Add Person</h3>
  </div>
  <div class="modal-body">
                
				<label>Name:</label> 
                <input type="text" style="width:514px;" name="fname" class="required" />
				
				<div>Type of message:</div>
				<label class="checkbox">
				<input type="checkbox"> Whatsup!
				</label>
				<label class="checkbox">
				<input type="checkbox"> Birthday | Select date:
				</label>
			</label>	
			
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    <button class="btn btn-primary">Save changes</button>
  </div>
  </form>
</div>

<!---------- Edit Person Modal ---------->
<div id="myModal-edit-person" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <form action="<?php echo site_url("page/update_person");?>" method="POST" id="milestoneformValidate"> 
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
    <h3 id="myModalLabel">Edit Person</h3>
  </div>
  <div class="modal-body"> 

				<label>Person:</label>

				
				<label>Name:</label> 
				<input type="text" name="fname" class="required" value="" ><?php echo $row->name; ?>
				
				<!---<p id="demo"></p>-->
				<input id="passhiddenID" type="hidden" name="getUID" class="required" />	
				
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    <button class="btn btn-primary">Save changes</button>
  </div></form>
</div>


<!---------- Message Modal ---------->
<div id="myModal-message" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <form action="<?php echo site_url("page/add_message");?>" method="POST" id="milestoneformValidate"> 
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
    <h3 id="myModalLabel">Add Message</h3>
  </div>
  <div class="modal-body"> 
 
				<label>Message:</label>
				<textarea rows="4" style="width:514px;" cols="30" name="fmessage" id="message" class="required"></textarea>
					
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    <button class="btn btn-primary">Save changes</button>
  </div></form>
</div>

<!---------- Edit Message Modal ---------->
<div id="myModal-edit-message" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <form action="<?php echo site_url("page/update_message");?>" method="POST" id="milestoneformValidate"> 
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
    <h3 id="myModalLabel">Edit Message</h3>
  </div>
  <div class="modal-body"> 

				<label>Message:</label>

				<textarea rows="4" style="width:514px;" cols="30" name="fmessage" id="entries" class="required"><?php echo $row->message; ?></textarea>

				<!---<p id="demo"></p>-->
				<input id="passhiddenID" type="hidden" name="getUID" class="required" />	
				
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    <button class="btn btn-primary">Save changes</button>
  </div></form>
</div>

  </body>
</html>