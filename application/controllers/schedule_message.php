<?php
 
// Set this to your timezone
date_default_timezone_set('America/New_York');
 
// Start at 8:00 AM (24-hour time)
$startTime = mktime(8, 0, 0);
 
// End at 5:00 PM (24-hour time)
$endTime = mktime(17, 0, 0);
 
 
$currentTime = time();
 
// Do not send the email if it is outside of the allowed hours
if($currentTime < $startTime || $currentTime > $endTime)
{
	print('Not sending an email after hours.');
	die();
}
 
// Get the current day of the week as an index (0=Sunday, 6=Saturday)
$dayOfWeek = date('w');
 
// Do not send the email on weekends
/* if($dayOfWeek == 0 || $dayOfWeek == 6)
{
	print('Not sending an email on the weekends.');
	die();
} */
 
// Info of person to receive the tests
define('TO_EMAIL',		'ttjordan81@gmail.com');
define('TO_NAME',		'John Doe');
 
// Info of person sending the tests
define('FROM_EMAIL',	'ttjordan81@yahoo.com');
define('FROM_NAME',	'Email Tester');
 
// Example: 8:00 am on 1 Nov 2010
$subject = 'Test: ' . date('g:i a \o\n j M Y');
 
$message = 'This email was automatically generated. Please send an email to yourusername@youremailprovider.com if you would like to disable these automated tests.';
 
$result = mail(TO_NAME . ' <' . TO_EMAIL . '>', $subject, $message, 'From: ' . FROM_NAME . ' <' . FROM_EMAIL . '>');
var_dump($result);