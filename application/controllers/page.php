<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page extends CI_Controller {

	public function index(){
	
		$data['whatsup_message'] = $this->get_model_message();
		$data['get_person'] = $this->get_model_person();
		$data['setting_view'] = $this->settings();
		$this->load->view('pages/home', $data);	
	}
	// get Message from Model
	public function get_model_message(){
	
		$this->load->model('Model_whatsup_message');
		$passMessage = $this->Model_whatsup_message->addmessageSelect();
	return $passMessage;
	}
	// Add new Messsage to database.
	public function add_message(){
		
		$getMessage = $_POST['fmessage'];
		
		$this->load->model('Model_whatsup_message');
		$this->Model_whatsup_message->add_messageInsert($getMessage);
		// After submiting the data echo this to return back to activity page.
		
		$this->session->set_flashdata('update', '<div class="alert alert-success">Successful, message was added!<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
		redirect('');

		//echo 'Message was Updated <a href="'.site_url().'">Return to Home</a> << TODO (CREATE VIEW)';
	}
	// get specify message  Whatusp from database
	public function get_message_whatsup(){
		$messageToeditID = $_GET["status"];
		
		$this->load->model('Model_whatsup_message');
		$entries = $this->Model_whatsup_message->get_edit_messageSelect($messageToeditID);
		echo $entries[0]->message;
	}
	// Update message.
	public function update_message(){
		
		$getMessage = $_POST['fmessage'];
		$getID = $_POST['getID'];
		
		$this->load->model('Model_whatsup_message');
		$this->Model_whatsup_message->add_messageUpdate($getMessage,$getID);
	
		// After submiting the data echo this to return back to activity page.
		$this->session->set_flashdata('update', '<div class="alert alert-success">Successful, message was updated!<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
		redirect('');

		//echo 'Message was Updated <a href="'.site_url().'">Return to Home</a> << TODO (CREATE VIEW)';
	}
	
	// function for profile settings
	private function settings(){		
		//$this->load->model('Model_settings');
		//$data['test'] = $this->Model_settings->checkSettings();
		$getSettingView = $this->load->view('pages/setting','', true);
	return $getSettingView;
	}
	// udpate time seeting
	public function settings_udpate(){
		// get start from input
		$email = $_POST['email'];
		$sTime = $_POST['sTime'];
		$eTime = $_POST['eTime'];
		
		$this->load->model('Model_settings');
		$this->Model_settings->checkSettings_udpate($email,$sTime,$eTime);	
		
		// After submiting the data echo this to return back to activity page.
		$this->session->set_flashdata('update', '<div class="alert alert-success">Successful, time was updated!<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
		redirect('');
	}
	
	// Add person to database.
	public function person(){
	
		$personName = $_POST['fname'];
		//$userID = $_POST['getUID']; // WORKON! Need to load user id

		$this->load->model('Model_person');
		$this->Model_person->person_name_add($personName);
		
		$this->session->set_flashdata('update', '<div class="alert alert-success">Successful, Name was added!<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
		redirect('');	
	}
	// get Person from Model
	public function get_model_person(){
	
		$this->load->model('Model_person');
		$passPerson = $this->Model_person->addpersonSelect();
	return $passPerson;
	}
	
}
